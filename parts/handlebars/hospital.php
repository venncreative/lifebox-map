<script id="hospital-template" type="text/x-handlebars-template">
  <div class="pop">
    <div class="pop-body center">
        <h2>{{name}}</h2>

        Oximeters Distributed:<br>
        <span class="oximiters-num">
             <span class="icon icon-plus-box" style="width: 1.5rem; height: 1.5rem;"></span>
            {{oximeters}}
        </span>
    </div>
  </div>
</script>
