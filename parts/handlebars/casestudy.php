<script id="casestudy-template" type="text/x-handlebars-template">
  <div class="pop">
    <div class="pop-head">
        <h3 class="pop-sub-title">Lifebox in</h3>
        <h2 class="pop-title">{{name}}</h2>
    </div>
    <div class="pop-body">
        <img src="{{image}}" alt="{{name}}" class="pop-image">
        <p>{{description}}</p>
    </div>
    <div class="pop-foot">
        <a href="{{link}}">Read Case Study &gt;</a>
    </div>
  </div>
</script>
