        <script id="country-template" type="text/x-handlebars-template">
          <div class="pop">
            <div class="pop-head">
                <img src="{{image}}" alt="{{name}}" class="pop-image">
                <h2 class="pop-title">{{name}}</h2>
                <p class="pop-population">Population: <span class="count">{{population}}</span></p>
            </div>

            {{#if has_stats}}
                <div class="pop-body">
                    <table class="pop-stats">

                        {{#if hospitals_partnered}}
                            <tr>
                                <td><span class="icon icon-hospitals"></span></td>
                                <td>Hospitals Partnered:</td>
                                <td>{{hospitals_partnered}}</td>
                            </tr>
                        {{/if}}

                        {{#if people_trained}}
                            <tr>
                                <td><span class="icon icon-people"></span></td>
                                <td>People Trained:</td>
                                <td>{{people_trained}}</td>
                            </tr>
                        {{/if}}

                        {{#if workshops}}
                            <tr>
                                <td><span class="icon icon-workshops"></span></td>
                                <td>Lifebox Workshops</td>
                                <td>{{workshops}}</td>
                            </tr>
                        {{/if}}

                    </table>
                </div>
            {{/if}}

            {{#if has_case_study}}
                <div class="pop-body">
                    <h6>Case Study</h6>
                    <p>{{case_study_description}}</p>

                    <a href="{{case_study_link}}" target="_blank" class="btn btn--full">Read More</a>
                </div>
            {{/if}}

            <div class="pop-body ">
            <span class="oximeters-label">
                 Oximeters Distributed:
            </span>
              
                <span class="oximiters-num">
                    <span class="icon icon-plus-box" style="width: 1.5rem; height: 1.5rem;"></span>
                    {{oximeters}}
                </span>
            </div>
            <div class="pop-foot js-zoom" href="#" data-zoom="6" data-lat="{{lat}}" data-long="{{long}}">
                <!-- <a class="js-zoom" href="#" data-zoom="6" data-lat="{{lat}}" data-long="{{long}}"> -->
                    Zoom In
                <!-- </a> -->
                <span class="fa fa-plus-circle" style="float: right;"></span>
            </div>
          </div>
        </script>
