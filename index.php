<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />

        <title>Mapbox</title>

        <meta name="viewport" content="width=device-width, minimum-scale=1.0, initial-scale=1.0" />

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href='//api.mapbox.com/mapbox.js/v2.4.0/mapbox.css' rel='stylesheet' />
        <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.css' rel='stylesheet' />
        <link href='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/MarkerCluster.Default.css' rel='stylesheet' />
        <link href="//fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/style.css" />

        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    </head>

    <body>
        <?php
            // 36.677231,67.115479 (lat,long)
            function input($key, $default = false) {
                if (isset($_GET[$key])) {
                    return $_GET[$key];
                }

                return $default;
            }
        ?>
        <div id="map" data-zoomto="<?php echo input('zoomto', 'false'); ?>"></div>

        <script src="https://npmcdn.com/imagesloaded@4.1/imagesloaded.pkgd.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min.js"></script>
        <script src='//api.mapbox.com/mapbox.js/v2.4.0/mapbox.js'></script>
        <script src='https://api.mapbox.com/mapbox.js/plugins/leaflet-markercluster/v0.4.0/leaflet.markercluster.js'></script>

        <script src='assets/js/Lifebox.js'></script>
        <script src='assets/js/map.js'></script>


        <?php // handlebar templates ?>
        <?php include 'parts/handlebars/casestudy.php'; ?>
        <?php include 'parts/handlebars/country.php'; ?>
        <?php include 'parts/handlebars/hospital.php'; ?>
    </body>
</html>
