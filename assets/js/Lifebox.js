/**
 * Lifebox
 *
 * Helper object for handling Map content
 */

(function(window, $, Handlebars, Leaflet) {

    // Create an empty object
    var Lifebox = {};

    // bind the mapbox map
    Lifebox.map = false;

    // layers holds an array of the
    // data layers in the map
    Lifebox.layers = {};

    // All compiled Handlebar templates
    Lifebox.templates = {};

    // Create a template and adds it to
    // the templates array
    Lifebox.addTemplate = function(name, sourceId) {
        this.templates[name] = Handlebars.compile( $(sourceId).html() );
    };

    // Create templates for Handlebars from an array
    Lifebox.addTemplates = function(templates) {
        for(var name in templates) {
            this.addTemplate(name, templates[name]);
        }
    };

    // Create HTML from template
    Lifebox.createTemplateHTML = function(name, data) {
        return this.templates[name](data);
    };

    // create a marker group for mapbox
    Lifebox.createMarkerGroup = function(markers) {

        // create empty array
        var group = [];

        // for each marker
        for(i = 0; i < markers.length; i++) {

            // create a basic marker at the coords
            var marker = Leaflet.marker(markers[i].coords);

            // turn it into geojson
            var data = marker.toGeoJSON();

            // add additional data to geojson
            data.properties = markers[i];

            // add marker to marker group
            group.push(data);
        }

        // return the array of markers
        return group;
    };

    // Create a layer and add it to the layers
    Lifebox.addLayer = function(name, url, options) {

        // first grab the data from the API url
        $.getJSON(url, function(data) {

            // create a maker group from the data
            // the API always return data wrapped in 'data'
            markers = Lifebox.createMarkerGroup(data['data']);

            // add the layer to the layers array
            Lifebox.layers[name] = new Leaflet.geoJson(markers, options);

            Lifebox.map.addLayer(Lifebox.layers[name]);

            // show/hide layers so the new layer will show once added
            Lifebox.toggleLayers();
        });
    };

    Lifebox.addLayerTwo = function(name, data, options) {

        // create a maker group from the data
        markers = Lifebox.createMarkerGroup(data);

        // add the layer to the layers array
        Lifebox.layers[name] = new Leaflet.geoJson(markers, options);

        // add the layer to the map
        Lifebox.map.addLayer(Lifebox.layers[name]);

        // show/hide layers so the new layer will show once added
        Lifebox.toggleLayers();
    };


    // show/hide layers depending on zoom
    Lifebox.toggleLayers = function() {

        for(var name in this.layers) {

            var layer = this.layers[name];
            var max = layer.options.maxZoom || false;
            var min = layer.options.minZoom || false;
            var z = Lifebox.map.getZoom();
            var show = false;

            // if both min and max value
            if (min && max) {

                // show the layer
                if(z <= max && z >= min) {
                    show = true;
                }

            // if only min
            } else if (min) {

                if(z >= min) {
                    show = true;
                }

            // if only max
            } else if (max) {

                if(z <= max) {
                    show = true;
                }
            }

            // if show and the map hasn't go the layer visible, show it
            if(show && ! Lifebox.map.hasLayer(layer)) {
                // console.log('add', name, 'to map');
                Lifebox.map.addLayer(layer);
            }

            // if hide and the map has the layer, hide it
            if(! show && Lifebox.map.hasLayer(layer)) {
                // console.log('remove', name, 'to map');
                Lifebox.map.removeLayer(layer);
            }
        }
    };

    // make object globally available
    // by binding it to window
    window.Lifebox = Lifebox;

})(window, jQuery, Handlebars, L);
