(function($, window, Lifebox) {

    $(document).ready(function() {

        // set access token
        L.mapbox.accessToken = 'pk.eyJ1IjoidmVubmNyZWF0aXZlIiwiYSI6ImNpbnZnd2R1NzAwbzZ3ZGx5MmxseHJraXMifQ.20KC29UdpgUA0gvPRE7ktQ';

        // initialise map
        var map = L.map('map', {
            center: [-5.615985819155327, 24.78515625],
            zoom: 2,
            minZoom: 0,
            maxZoom: 21,
            zoomControl: false,
            scrollWheelZoom: false,
            worldCopyJump: true
        });

        new L.Control.Zoom({ position: 'bottomleft' }).addTo(map);

        // add style layer
        // L.mapbox.styleLayer('mapbox://styles/venncreative/civi5a9jx00a62jqlyt4zjzh0').addTo(map);
        L.mapbox.styleLayer('mapbox://styles/venncreative/cj1xws0kz00082smnrz7be2aw').addTo(map);

        // When the map is ready, set everything up
        map.whenReady(function() {

            // lifebox object available in global scope and passed
            Lifebox.map = map;

            // create icons for map markers
            var icons = {};

            // create case study icon
            icons['casestudy'] = L.divIcon({
                className: "marker-casestudy",
                iconSize: [36, 39]
            });

            // create country icon
            icons['country'] = L.divIcon({
                className: "marker-country",
                iconSize: [16, 16],
            });

            // hospital icons is a function
            // we need to pass the content of the icon
            // when it's created
            icons['hospital'] = function(html) {

                // if no html is set, make it blank
                var content = html | '';

                // return a new div icon with the html content
                return L.divIcon({
                    className: "marker-hospital",
                    iconSize: [21, 21],
                    html: content,
                });
            };

            // Create all Handlebar templates for popups
            Lifebox.addTemplates({
                'casestudy': '#casestudy-template',
                'country': '#country-template',
                'hospital': '#hospital-template'
            });


            // get country markers from api
            $.getJSON('api/countries', function(data) {

                // add the layer to the markers
                Lifebox.addLayerTwo('countries', data.data, {
                    // add custom properties to layer?
                    name: 'countries',
                    maxZoom: 5,
                    pointToLayer: function(feature, latlng) {

                        var icon = icons['country'];

                        if (feature.properties.has_case_study) {
                            icon = icons['casestudy'];
                        }

                        // have to create a new div icon
                        // to add the number to the icon
                        var marker = L.marker(latlng, {
                            icon: icon
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {

                        // when the layer is clicked
                        layer.on('click', function(e) {

                            // wrap content for the popup in jquery
                            var $content = $( Lifebox.createTemplateHTML('country', feature.properties) );

                            // create a new popup
                            var pop = L.popup()
                                .setLatLng(feature.properties.coords)
                                // set the content as a loading spinner
                                .setContent('<div class="pop pop--loading"><span class="fa fa-spin fa-circle-o-notch"></span></div>')
                                .openOn(map);

                            // when the content has finished loading (images)
                            $content.imagesLoaded(function() {
                                // set the content and update the popup, this will reposition the map etc
                                pop.setContent($content.html()).update();
                                // add class the change the tip colour to red
                                $('.leaflet-popup-tip').addClass('tip--red');
                            });

                            // set the active popup
                            Lifebox.activePop = pop;
                        });
                    }
                });
            });

            // get all hospital markers from api
            $.getJSON('api/all', function(data, status) {
                // add to layer
                Lifebox.addLayerTwo('all', data.data, {
                    // add custom properties to layer?
                    name: 'all',
                    minZoom: 6,
                    pointToLayer: function(feature, latlng) {
                        // create hospital div icon, passing the number of oximeters
                        var i = icons['hospital'](feature.properties.oximeters);

                        // have to create a new div icon
                        // to add the number to the icon
                        var marker = L.marker(latlng, {
                            icon: i
                        });

                        return marker;
                    },
                    onEachFeature: function(feature, layer) {
                        // bind a popup to the marker
                        layer.bindPopup( Lifebox.createTemplateHTML('hospital', feature.properties) );
                    }
                });
            });


            // when the user zooms
            // determine whether or not to show a layer
            map.on('zoomend', function() {
                Lifebox.toggleLayers();
            });

            //zoom out on mobile
            var width = document.documentElement.clientWidth;
                // tablets are between 768 and 922 pixels wide
                // phones are less than 768 pixels wide
                if (width < 768) {
                    // set the zoom level to 0
                    map.setZoom(1);
                }  else {
                    // set the zoom level to 8
                    map.setZoom(2);
                }
            // listen for screen resize events
            window.addEventListener('resize', function(event){
                // get the width of the screen after the resize event
                var width = document.documentElement.clientWidth;
                // tablets are between 768 and 922 pixels wide
                // phones are less than 768 pixels wide
                if (width < 768) {
                    // set the zoom level to 0
                    map.setZoom(1);
                }  else {
                    // set the zoom level to 8
                    map.setZoom(2);
                }
            });

            $(document).on('click', '.js-zoom', function(e) {
                e.preventDefault();

                var zoom = $(this).attr('data-zoom');
                var lat = $(this).attr('data-lat');
                var long = $(this).attr('data-long');

                map.setView([long, lat], zoom);

                // close the popup after
                // this is set in feature group for countries
                Lifebox.activePop._close();
            });

            // check for a specific marker by coords in data attribute
            // example data-zoomto="39.513086,47.043264"
            var latLong = $('#map').data('zoomto');

            // if the latLong is not false
            if (latLong) {
                // get the correct values for lat/long
                var lat = parseFloat(latLong.split(',')[0]);
                var long = parseFloat(latLong.split(',')[1]);

                // center the view at the marker
                map.setView([lat, long], 7);

                // create the highlight marker
                var c = L.circleMarker([lat, long], {
                    radius: 18,
                    stroke: true,
                    // color: '#f46243',
                    color: '#ffeb00',
                    weight: 5,
                    opacity: 0.9,
                    fill: false,
                    fillColor: '#f46243',
                    fillOpacity: 0.2
                });

                // Create a layer for the highlighted marker
                Lifebox.layers['highlight'] = L.layerGroup([c]);
                Lifebox.layers['highlight'].options = {
                    minZoom: 6 // same as hospital markers
                };

                // force toggle layers
                Lifebox.toggleLayers();
            }

        });
    });

})(jQuery, window, Lifebox);
