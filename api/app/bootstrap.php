<?php

// Force errors to display
// should remove on live site
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

// require autoloader
require __DIR__ . '/../vendor/autoload.php';

// bring in dependencies needed
use Slim\App;
use App\Services\Cache;
use App\Services\GoogleSpreadsheet;
use League\Fractal\Manager as Fractal;

// create new slim app
$app = new App;

// get the slim container
$container = $app->getContainer();


// bind google spreadsheets class
$container['google'] = function() {

    // create google spreadsheet object
    $google = new GoogleSpreadsheet([
        "app" => "Lifebox-Map",
        "key" => "AIzaSyBXTwK9jrcI3AYru4tdL7JiXE3Am0nXoyw",
        "file" => "1cF6fO4sEnLIVm_mCmymCoi11qQWE78OG66FTzD7IPdU"
    ]);

    return $google;
};



// bind fractal manager to the container
$container['fractal'] = function() {
    return new Fractal;
};

$container['cache'] = function() {
    return new Cache([
        'path' => __DIR__ . '/cache/'
    ]);
};



// add routes to slim
require __DIR__ . '/Routes/routes.php';
