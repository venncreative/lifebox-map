<?php

namespace App\Transformers;

use \League\Fractal\TransformerAbstract;

class ContinentTransformer extends TransformerAbstract
{
    public function transform($continent)
    {
        return [
            'name' => $continent->name,
            'hospitals' => (int) $continent->hospitals,
            'oximeters' => (int) $continent->oximeters,
            'long' => (float) $continent->long,
            'lat' => (float) $continent->lat,
            'coords' => [
                (float) $continent->long,
                (float) $continent->lat
            ]
        ];
    }
}
