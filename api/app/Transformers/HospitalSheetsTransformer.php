<?php

namespace App\Transformers;

use \League\Fractal\TransformerAbstract;

class HospitalSheetsTransformer extends TransformerAbstract
{
    public function transform($sheet)
    {
        return [
            'id' => $sheet->id,
            'name' => $sheet->title,
            'slug' => $sheet->slug
        ];
    }
}
