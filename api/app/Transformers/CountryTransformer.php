<?php

namespace App\Transformers;

use \League\Fractal\TransformerAbstract;

class CountryTransformer extends TransformerAbstract
{
    public function transform($country)
    {
        // set original urls as defaults for images
        $image = $country->image;
        $image_large = $country->image_large;

        // get the proper file url for the image
        if ($imageFile = $this->lifeboxImage($country->image)) {
            $image = $imageFile;
        }

        if ($imageFileLarge = $this->lifeboxImage($country->image_large)) {
            $image_large = $imageFileLarge;
        }

        // check if all stats are set
        $has_stats = true;

        // if all stats are zero
        if (
            (int) $country->hospitals_partnered < 1 &&
            (int) $country->people_trained < 1 &&
            (int) $country->workshops < 1
        ) {
            $has_stats = false;
        }

        $has_casestudy = false;

        // if we have case studies
        if (
            isset($country->case_study_link) &&
            !empty($country->case_study_link)
        ) {
            $has_casestudy = true;
        }

        return [
            'name' => $country->name,
            'image' => $image,
            'image_large' => $image_large,
            'population' => $country->population,
            'has_stats' => $has_stats,
            'hospitals_partnered' => (int) $country->hospitals_partnered,
            'people_trained' => (int) $country->people_trained,
            'workshops' => (int) $country->workshops,
            'oximeters' => (int) $country->oximeters,
            'has_case_study' => $has_casestudy,
            'case_study_link' => $country->case_study_link,
            'case_study_description' => $country->case_study_description,
            'long' => (float) $country->longitude,
            'lat' => (float) $country->latitude,
            'coords' => [
                (float) $country->longitude,
                (float) $country->latitude
            ]
        ];
    }

    // takes the google share url and creates the file url
    public function lifeboxImage($shareUrl)
    {
        // modify the image urls to work with google drive
        $url = 'https://drive.google.com/uc?export=view&id={photoId}';

        // extract the query string from the url
        if ($query = parse_url($shareUrl, PHP_URL_QUERY)) {

            // parse the query string into an array
            parse_str($query, $output);

            // if there is an id
            if (isset($output['id'])) {

                // change placeholder in url to the ID
                return str_replace('{photoId}', $output['id'], $url);
            }
        }

        // if all else fails, return false
        return false;
    }
}
