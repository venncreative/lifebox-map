<?php

namespace App\Transformers;

use \League\Fractal\TransformerAbstract;

class CaseStudyTransformer extends TransformerAbstract
{
    public function transform($casestudy)
    {

        return [
            'name'    => $casestudy->name,
            'description' => $casestudy->description,
            'image'  => $casestudy->image,
            'longitude' => $casestudy->longitude,
            'latitude' => $casestudy->latitude,
            'coords' => [
                $casestudy->longitude,
                $casestudy->latitude,
            ]
        ];
    }
}
