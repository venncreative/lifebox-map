<?php

namespace App\Transformers;

use \League\Fractal\TransformerAbstract;

class HospitalTransformer extends TransformerAbstract
{
    public function transform($hospital)
    {
        // Lifebox changed 'name' column to 'hospital'
        $name = '';

        if (isset($hospital->name)) {
            $name = $hospital->name;
        } elseif (isset($hospital->hospital)) {
            $name = $hospital->hospital;
        }

        // Lifebox changed 'oximeters' to 'oximeter'
        $oximeters = 0;

        if (isset($hospital->oximeters)) {
            $oximeters = $hospital->oximeters;
        } elseif (isset($hospital->oximeter)) {
            $oximeters = $hospital->oximeter;
        }

        return [
            'name' => $name,
            'oximeters' => (int) $oximeters,
            'country' => $hospital->country,
            'lat' => (float) $hospital->latitude,
            'long' => (float) $hospital->longitude,

            // For whatever reason, some API's require coordinates passed
            // in a very sepcific order, som lat/long others long/lat
            // depsite using GeoJSON for all marker groups
            // when using GeoJSON with cluster groups
            // it needs to be provided lat/long
            // where as others are long/lat
            //
            // https://www.mapbox.com/help/define-lat-lon/
            'coords' => [
                (float) $hospital->latitude,
                (float) $hospital->longitude
            ]
        ];
    }
}
