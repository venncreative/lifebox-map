<?php

namespace App;

class Spreadsheet {

    /**
     * The spreadsheet file ID
     * @var string
     */
    public $fileId;

    /**
     * An array of sheet IDs in the spreadsheet
     * @var array
     */
    public $sheets;

    /**
     * Create spreadsheet object with the file ID
     * @param string $fileId the spreadsheet ID
     */
    public function __construct($fileId)
    {
        $this->file($fileId);
    }

    /**
     * Set the spreadsheet file ID
     * @param  string $fileId the spreadsheet file ID
     */
    public function file($fileId)
    {
        $this->fileId = $fileId;
    }

    /**
     * Pass an array of sheet IDs within the spreadsheet
     * @param  array  $sheets
     */
    public function sheets($sheets = [])
    {
        $this->sheets = $sheets;
    }

    /**
     * Get data from a sheet as JSON
     * @param  string $sheetName the sheet name
     * @return string
     */
    function get($sheetName)
    {

        $csv = $this->getCSV($sheetName);

        return $json = $this->CSVtoJson($csv);
    }

    /**
     * Get the sheet csv data from Google
     * @param  string $sheetName
     */
    private function getCSV($sheetName)
    {

        // if using custom name for sheet
        if (isset($this->sheets[$sheetName])) {
            // get the sheet ID
            $sheetName = $this->sheets[$sheetName];
        }

        $query = [
            'output' => 'csv',
            'gid' => $sheetName
        ];

        $query_string = http_build_query($query);

        // build the URL for Google Spreadsheet
        $url = 'https://docs.google.com/spreadsheets/d/' . $this->fileId . '/pub?' . $query_string;

        // return the csv data
        return file_get_contents($url);
    }

    /**
     * Convert CSV to JSON
     * @param string $csv
     */
    private function CSVtoJson($csv)
    {
        // turn string into array of each row in csv
        // each row is still a string
        $data = explode(PHP_EOL, $csv);

        // keys are the first row, so take that out the array
        $keys = array_shift($data);

        // turn the keys into an array
        $keys = str_getcsv( str_replace(' ', '_', strtolower($keys) ) );

        // create empty array for all content
        $collection = [];

        // for each line of data
        foreach($data as $values) {
            // turn csv string for row into array
            $values = str_getcsv($values);

            // assign the keys to the data array
            // add to new content
            $data = (object) array_combine($keys, $values);


            // ignore values with no lat/long
            if (empty($data->latitude) || empty($data->longitude)) {
                continue;
            }

            // add new data to the collection
            $collection[] = $data;
        }

        // return data collection
        return $collection;
    }

}

