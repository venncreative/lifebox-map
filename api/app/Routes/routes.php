<?php

use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;

use App\Transformers\CountryTransformer;
use App\Transformers\CaseStudyTransformer;
use App\Transformers\ContinentTransformer;
use App\Transformers\HospitalTransformer;
use App\Transformers\HospitalSheetsTransformer;

use GuzzleHttp\Client;
use GuzzleHttp\Promise;


/**
 * Countries Routes
 */
$app->get('/countries', function($request, $response, $args) {

    if ($this->cache->expired('countries')) {

        // get the data from google
        $countries = $this->google->sheet('countries')->get();

        // create a resource for the api
        $resource = new Collection($countries, new CountryTransformer);

        // transform the data?
        $data = $this->fractal->createData($resource)->toArray();

        $this->cache->save('countries', $data);

    } else {
        // load country data from cache
        $data = $this->cache->load('countries');
    }

    // return the json response
    return $response->withJson($data);
});

/**
 * Hospitals Routes
 */
$app->get('/hospitals', function($request, $response) {

    // get a list of all the country sheets
    $countries = $this->google->getCountries();

    // create a collection for the countries
    // transforming the data into a uniform format
    $resource = new Collection($countries, new HospitalSheetsTransformer);

    // create the API response
    $data = $this->fractal->createData($resource)->toArray();

    // return the response
    return $response->withJson($data);
});

/**
 * Individual countries hospital markers endpoint
 */
$app->get('/hospitals/{slug}', function($request, $response, $args) {

    // get the sheet slug we want hospitals from
    $slug = $args['slug'];

    // if the sheet exists
    if ($this->google->sheets()->has($slug)) {

        // check if cache is available
        if ($this->cache->expired('hospitals-' . $slug)) {

            // get the sheet data from source (google)
            $hospital_markers = $this->google->sheet($slug)->get();

            // create a resource for the api
            $resource = new Collection($hospital_markers, new HospitalTransformer);

            // transform the data?
            $data = $this->fractal->createData($resource)->toArray();

            // save to cache
            $this->cache->save('hospitals-' . $slug, $data);

        } else {
            // load country data from cache
            $data = $this->cache->load('hospitals-' . $slug);
        }

    // Return an error
    } else {

        // set status to 404
        $response = $response->withStatus(404);

        // create the API response array
        $data = [
            'error' => 1,
            'data' => [
                'error' => "Cannot find sheet ${slug}"
            ]
        ];
    }

    return $response->withJson($data);
});


/**
 * All hospital markers endpoint, in one large response
 */
$app->get('/all', function($request, $response, $args) {

    // check if cache exists, or if it has expired
    if ($this->cache->expired('all')) {

        // get a list of the country hospital marker sheets
        $countries = $this->google->getCountries();

        // create a new guzzle client
        $client = new Client([
            'base_uri' => 'https://docs.google.com/spreadsheets/d/1cF6fO4sEnLIVm_mCmymCoi11qQWE78OG66FTzD7IPdU/'
        ]);

        // start an array of promises
        $promises = [];

        // create async requests for each sheets endpoint
        foreach ($countries as $sheet) {
            $promises[$sheet->slug] = $client->requestAsync('GET', 'pub', [
                'query' => [
                    'output' => 'csv',
                    'gid' => $sheet->id
                ]
            ]);
        }

        // save google object to local object
        $google = $this->google;

        // create an empty data arary
        $data = [];

        // define how to handle all promises
        $data = Promise\all($promises)->then(function($responses) use ($data, $countries, $google) {
            // when promises are complete, cycle through each
            foreach ($responses as $slug => $response) {

                // get the csv data
                $csv = $response->getBody()->getContents();

                // convert the csv to an array
                $arr = $google->CSVtoArray($csv);

                // cycle through all markers and add the country to the data
                $arr = array_map(function ($item) use ($countries, $slug) {
                    // set the items country via the slug set earlier
                    $item->country = $countries[$slug]->title;

                    return $item;
                }, $arr);

                // merge array in to one large array
                $data = array_merge($data, $arr);
            }

            // return the data
            return $data;
        })->wait();

        // transform the data into a uniform format for markers
        $resource = new Collection($data, new HospitalTransformer);

        // transform the data to what we want to return in the api
        $data = $this->fractal->createData($resource)->toArray();

        // save to cache
        $this->cache->save('all', $data);

    } else {
        // load data from cache
        $data = $this->cache->load('all');
    }

    return $response->withJson($data);
});
