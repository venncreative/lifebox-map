<?php

namespace App\Services;

class Cache
{

    public $options;

    public function __construct($options = [])
    {
        $defaults = [
            'path' => '/cache/',
            'expires' => 60 * 60 * 24 // 1 day
        ];

        $this->options = array_merge($defaults, $options);
    }

    /**
     * Load data from file
     * @param  string $file
     */
    public function load($file)
    {

        if ($this->exists($file)) {
            return json_decode(file_get_contents($this->fullPath($file)));
        }

        return false;
    }

    /**
     * Save data to file
     * @param  [type] $file [description]
     * @return [type]       [description]
     */
    public function save($file, $data = [])
    {
        $file = fopen($this->fullPath($file), 'w');
        fwrite($file, json_encode($data));
        fclose($file);
    }

    /**
     * Check the file exists
     * @param  string $file
     */
    public function exists($file)
    {
        return file_exists($this->fullPath($file));
    }

    /**
     * Check the file has not expired
     * @param  string $file
     */
    public function expired($file, $expires = null)
    {
        $path = $this->fullPath($file);

        // if the file does not exists
        if (!$this->exists($file)) {
            // make it as if it has expired
            return true;
        }

        // if no time passed use default from options
        if (is_null($expires)) {
            $expires = $this->options['expires'];
        }

        // get the files made time
        $made = filemtime($path);

        // if the file has expired
        if ((time() - $made) > $expires) {
            return true;
        }

        return false;
    }

    public function fullPath($file)
    {
        return $this->options['path'] . $file . '.json';
    }
}
