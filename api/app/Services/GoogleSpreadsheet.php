<?php

namespace App\Services;

use Illuminate\Support\Collection;
use \Google_Client;
use \Google_Service_Sheets;

/**
 * Google Spreadsheet Class
 *
 * Way to interact with data in
 * a Google Drive Spreadsheet
 */
class GoogleSpreadsheet {

    /**
     * Create a Google Spreadsheet object
     * Options:
     *     - app = Google app name
     *     - key = Google app key
     *     - file = The file ID for spreadsheet
     */
    public function __construct($options)
    {
        // set the config optipns
        $this->config = $options;

        // if we have an app and key, create a client
        if (isset($options['app']) && isset($options['key'])) {

            // create a google spreadsheet client
            $this->createClient($options['app'], $options['key']);

            // boot the rest of the object
            $this->boot();
        }
    }

    /**
     * Return the current Google Spreadsheet API service
     * for the current file id
     */
    public function service()
    {
        // return the service for use
        return $this->service;
    }

    /**
     * Get a collection of sheets in the file
     * @return Collection
     */
    public function sheets()
    {
        return $this->sheets;
    }

    /**
     * Define the sheet we want data from
     */
    public function sheet($slug)
    {
        // find the sheet id of the slig
        if ($this->sheets()->has($slug)) {
            // save the sheet ID
            $this->_sheet = $this->sheets()->get($slug)->id;
        } else {
            $this->_sheet = null;
        }

        // return current object for chainability
        return $this;
    }

    /**
     * Get the sheets data
     */
    public function get()
    {

        // if the sheet is not set return an empty collection
        if (is_null($this->_sheet)) {
            return new Collection([]);
        }

        // get the sheet id called earlier
        $sheet = $this->_sheet;

        // get the file from config
        $file = $this->config['file'];

        // get the csv
        $csv = $this->getCSV($file, $sheet);

        // turn CSV to a PHP Array
        $data = $this->CSVtoArray($csv);

        // return a collection of the data returned
        return new Collection($data);
    }


    /**
     * The following items are for internal use
     */

    /**
     * Create a Google Client with the credentials
     * passed in the constructor
     * @param  string $app The Google app name
     * @param  string $key The Google app key
     */
    private function createClient($app, $key)
    {
        // create google client
        $this->client = new Google_Client();
        $this->client->setApplicationName($app);
        $this->client->setDeveloperKey($key);

        // use client inside service and set service
        $this->service = new Google_Service_Sheets($this->client);
    }

    /**
     * Do some additional work on construct
     */
    private function boot()
    {
        // set the file from the config using the client
        $this->file = $this->service()->spreadsheets->get($this->config['file']);

        // create a collection of spreadsheet to reference by slug
        $this->createSheets();
    }

    /**
     * Creates an array of spreadsheet slugs => id
     */
    private function createSheets()
    {
        // get a list of sheets from google
        $sheets = $this->file->getSheets();

        // create empty array
        $sheet_array = [];


        // foreach sheet
        foreach ($sheets as $sheet) {

            // get the sheets properties
            $sheet = $sheet->getProperties();

            // create a slug
            $slug = $this->slugify($sheet->title);

            // create data for the sheet
            $data = [
                'id' => $sheet->sheetId,
                'title' => $sheet->title,
                'slug' => $slug
            ];

            // create an array of sheet slugs => sheet id
            $sheet_array[$slug] = (object) $data;
        }

        // sort alphabetically
        ksort($sheet_array);

        // turn sheets into a collection
        $this->sheets = new Collection($sheet_array);
    }

    /**
     * Used to turn a title into a url friendly slug
     * @param  string $title Title string
     * @return string        Slugified version of string
     */
    private function slugify($title)
    {
        return strtolower( str_replace(' ', '-', $title) );
    }

    /**
     * Get CSV data for a spreadsheet
     * @param  string $file
     * @param  string $sheet
     * @return string CSV string
     */
    private function getCSV($file, $sheet)
    {
        $query = [
            'output' => 'csv',
            'gid' => $sheet
        ];

        $query_string = http_build_query($query);

        // build the URL for Google Spreadsheet
        $url = 'https://docs.google.com/spreadsheets/d/' . $file . '/pub?' . $query_string;

        // return the csv data
        return file_get_contents($url);
    }

    /**
     * Convert a CSV string into an array
     * @param string $csv
     */
    public function CSVtoArray($csv)
    {
        // turn string into array of each row in csv
        // each row is still a string
        $data = explode(PHP_EOL, $csv);

        // keys are the first row, so take that out the array
        $keys = array_shift($data);

        // turn the keys into an array
        $keys = str_getcsv( str_replace(' ', '_', strtolower($keys) ) );

        // create empty array for all content
        $collection = [];

        // for each line of data
        foreach($data as $values) {

            // turn csv string for row into array
            $values = str_getcsv($values);

            // if there aren't enough values, skip the item
            if (count($keys) !== count($values)) {
                continue;
            }

            // assign the keys to the data array
            // add to new content
            $data = (object) array_combine($keys, $values);


            // ignore values with no lat/long
            if (empty($data->latitude) || empty($data->longitude)) {
                continue;
            }

            // add new data to the collection
            $collection[] = $data;
        }

        // return data collection
        return $collection;
    }

    public function getCountries()
    {
        return $this->sheets()->filter(function($value, $key) {
            // return items that are not hospital marker data
            return ! in_array($value->title, [
               'Case Studies',
               'Continents',
               'Countries',
               'Hospitals Example'
            ]);

        })->all();
    }
}
